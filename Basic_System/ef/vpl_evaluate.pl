#!/usr/bin/perl -w
use strict;
use File::Slurp;
use File::Temp;
use File::Copy;
use Data::Dumper;
use Errno;
use IPC::Open3;
use Symbol 'gensym';
use Fcntl;

our %mode;
our %colors = (
    BLUE => "\033[34m",
    GREEN => "\033[32m",
    RED => "\033[31m",
    YELLOW => "\033[33m",
    BB => "\033[40m",
    NC => "\033[0m"
    );

# *** FUNCTIONS ***

sub colored_output($@) {
    my $color = shift;
    if ($mode{local}) {
        print "$colors{$color}";
    }
    print @_;
    if ($mode{local}) {
        print "$colors{NC}";
    }
}

sub comment($) {
    my $line = shift;
    if ($mode{local}) {
        print $line, "\n";
    } else {
        print "<|--\n", $line, "\n--|>\n";
    }
}

sub title($) {
    if ($mode{local}) {
        colored_output('BLUE', shift."\n");
    } else {
        comment("-".shift);
    }
}

sub grade($) {
    my $score = sprintf("%.2f", shift);
    hidden("Grade :=>>$score\n");
}

sub error($) {
    if ($mode{local}) {
        colored_output('RED', "Internal error : ");
    } else {
        title("Internal error");
    }
    comment(shift);
    exit 1;
}

sub warning(@) {
    colored_output('RED', @_);
}

sub debug(@) {
    colored_output('GREEN', @_) if $mode{debug};
}

sub hidden(@) {
    colored_output('YELLOW', @_);
}

sub remove(@) {
    return unlink(@_) unless $mode{debug};
    return 1;
}

# Gets a named field from a hash if it exists, otherwise substitute a given default
sub get_value($$$) {
    my $description = shift;
    my $name = shift;
    my $default = shift;
    if ($description && exists($description->{$name})) {
        return $description->{$name};
    } else {
        return $default;
    }
}

sub sets_create() {
    my $sets = {};
    $sets->{left} = 0;
    $sets->{avail} = {};
    $sets->{read} = $sets->{write} = $sets->{except} = '';
    return $sets;
}

sub sets_empty($) {
    my $sets = shift;
    return $sets->{left} == 0;
}

sub sets_add($$$) {
    my $sets = shift;
    my $set = shift;
    my $stream = shift;
    $sets->{$stream} = $set;
    my $fd = fileno($stream);
    vec($sets->{$set}, $fd,  1) = 1;
    vec($sets->{except}, $fd,  1) = 1;
    $sets->{left}++;
}

sub sets_select($) {
    my $sets = shift;
    my $avail = $sets->{avail};
    $avail->{read} = $sets->{read};
    $avail->{write} = $sets->{write};
    $avail->{except} = $sets->{except};
    my $nfound = select($avail->{read}, $avail->{write}, $avail->{except}, undef);
    if ($nfound == -1) {
        debug("Unexpected select error $!\n");
    }
}

sub sets_avail($$) {
    my $sets = shift;
    my $stream = shift;
    my $set = $sets->{$stream};
    my $fd = fileno($stream);
    if (vec($sets->{avail}->{except}, $fd, 1)) {
        debug("Unexpected exceptionnal condition on command fd $fd with $set access\n");
    }
    return vec($sets->{avail}->{$set}, $fd, 1);
}

sub sets_access($$$$$) {
    my $sets = shift;
    my $stream = shift;
    my $buffer = shift;
    my $length = shift;
    my $offset = shift;
    my $set = $sets->{$stream};

    if ($set eq 'read') {
        return sysread($stream, $$buffer, $length, $offset);
    } else {
        return syswrite($stream, $$buffer, $length, $offset);
    }
}

sub sets_remove($$) {
    my $sets = shift;
    my $stream = shift;
    my $set = $sets->{$stream};
    my $fd = fileno($stream);
    vec($sets->{$set}, $fd, 1) = 0;
    vec($sets->{except}, $fd, 1) = 0;
    $sets->{left}--;
    close($stream);
    # debug("$set stream $stream (fd $fd) removed\n");
}

# Runs a command associated to a test with a given input, returns a hash that holds the content of standard output and error as well as exit code
# Looks at outputs_limit, timeout, debug, args and input in the test to find out how to run it (but they are not necessarily expected to exist)
sub run_command($$$$) {
    my $test_name = shift;
    my $command = shift;
    my $test = shift;
    my $has_limits = shift;
    my @arguments = ();
    my $timeout;

    chmod(0755, "$command") || error("Cannot make $command executable : $!".
                                     (exists($test->{compile})?"":" (and no 'compile' directive found)"));
    if (exists($test->{args})) {
        @arguments = @{$test->{args}};
    }
    debug("Executing $command");
    if ($has_limits) {
        $timeout = get_value($test, 'timeout', 10);
        if ($timeout > 0) {
            unshift @arguments, '-s9', $timeout, $command;
            $command = 'timeout';
        }
    }

    # create the process with open3 in order to store its outputs/errors in memory
    my $child = {};
    # error has to exists otherwise open3 merge output and error into the same stream
    $child->{error} = gensym;
    $SIG{PIPE} = 'DEFAULT';
    my $pid = open3($child->{input}, $child->{output}, $child->{error}, $command, @arguments) || error("Cannot exec $command, error $!\n");
    $SIG{PIPE} = 'IGNORE';

    my $remaining = {};
    my $done = {};
    my $data = {};
    # We require non blocking IO to avoid deadlocks
    for my $part ('input', 'output', 'error') {
        # using select, it should not be necessary to make the file descriptor non blocking
        # fcntl($child->{$part}, F_SETFL, O_NONBLOCK) || error("Cannot make $part descriptor non blocking");
        if ($has_limits) {
            $remaining->{$part} = get_value($test, "${part}_limit", 10*1024);
        } else {
            $remaining->{$part} = -1;
        }
        $done->{$part} = 0;
        $data->{$part} = '';
    }
    $data->{input} = get_value($test, 'input', "");
    $remaining->{input} = length($data->{input});
    if ($has_limits) {
        my $output_msg = ($remaining->{output} >= 0) ? "limited to $remaining->{output} bytes":"unlimited";
        my $error_msg = ($remaining->{error} >= 0) ? "limited to $remaining->{error} bytes":"unlimited";
        my $timeout_msg = ($timeout > 0) ? "timeout at ${timeout}s":"no timeout";
        debug(" with output $output_msg, error $error_msg and $timeout_msg");
    }
    debug("\n");

    # we use select to perform I/Os with the child (still in order to avoid deadlocks)
    my $sets = sets_create();
    if ($remaining->{input}) {
        sets_add($sets, 'write', $child->{input});
    } else {
        close($child->{input});
        delete($child->{input});
    }
    sets_add($sets, 'read', $child->{output});
    sets_add($sets, 'read', $child->{error});

    while (!sets_empty($sets)) {
        sets_select($sets);
        for my $part (keys(%$child)) {
            if (sets_avail($sets, $child->{$part})) {
                my $number;
                my $size = $remaining->{$part};
                $size = 1024 if $size == -1;
                $number = sets_access($sets, $child->{$part}, \$data->{$part}, $size, $done->{$part});
                if (!defined($number)) {
                    if ($!{EINTR} || $!{EAGAIN} || $!{EWOULDBLOCK}) {
                        debug("Access to command $part temporarily failed for reason $!, ".
                              "errors EINTR $!{EINTR} EAGAIN $!{EAGAIN} EWOULDBLOCK $!{EWOULDBLOCK}, ".
                              "size $size done $done->{$part}\n");
                    } else {
                        debug("Error $! during access to command $part\n");
                        sets_remove($sets, $child->{$part});
                        delete($child->{$part});
                    }
                } else {
                    if ($number) {
                        $remaining->{$part} -= $number unless $remaining->{$part} == -1;
                        $done->{$part} += $number;
                    }
                    if (!$number || !$remaining->{$part}) {
                        sets_remove($sets, $child->{$part});
                        delete($child->{$part});
                    }
                    if (!$remaining->{$part}) {
                        $data->{"${part}_bounded"} = 1;
                    }
                }
            }
        }
    }

    delete($data->{input});
    delete($data->{input_bounded});
    waitpid($pid, 0) >= 0 || error("Wait for command termination failed\n");
    # One should use the POSIX macros WIFEXITED, WEXITSTATUS, and so on, but it's too cumbersome
    $data->{failure} = $?;
    $data->{signal} = $data->{failure} & 0xFF;
    $data->{code} = $data->{failure} >> 8;
    # debug("Result : ".Dumper($data));
    return $data;
}

sub setup_env($$) {
    my $name = shift;
    my $program = shift;

    # ARGS is not necessary as it is accessible through $@
    foreach my $part ('files') {
        if (exists($program->{$part})) {
            my $variable = uc($part);
            $ENV{$variable} = join(" ", @{$program->{$part}});
        }
    }
    $ENV{NAME} = $name;
}

sub dispatch_error($$$) {
    my $message = shift;
    my $data = shift;
    my $soft_fail = shift;

    warning($data->{output}) if length($data->{output});
    warning($data->{error}) if length($data->{error});
    if ($soft_fail) {
        warning("$message\n");
    } else {
        error($message);
    }
}

# Runs a named command, described by a given content and associated to a test,
# returns a hash that holds the content of standard output and error as well as exit code
# The content might be :
# - scalar : a script to be executed
# - a ref to a hash : description of the executable and execution
# The test holds input and args informations
sub run_special($$$$$);
sub run_special($$$$$) {
    my $test_name = shift;
    my $name = shift;
    my $content = shift;
    my $test = shift;
    my $has_limits = shift;
    my $special = $content;

    if (ref($special) ne 'HASH') {
        $special = { content => $content };
    } else {
        # Values of special are written to test to be able to overwrite intput and args in special
        # This is especially useful for a validator
        my $given_test = $test;
        $test = {};
        add_values($test, $given_test);
        add_values($test, $special);
    }
    # generate file if required and populate the 'files' array
    if (exists($special->{content})) {
        if (!exists($special->{filename})) {
            $special->{filename} = $name;
        }
        ensure_write($special->{filename}, $special->{content});
        if (!exists($special->{files})) {
            $special->{files} = [ $special->{filename} ];
        } elsif (!scalar(grep(/$special->{filename}/, @{$special->{files}}))) {
            unshift @{$special->{files}}, $special->{filename};
        }
    }
    # compile if required
    if (exists($special->{compile})) {
        if (exists($special->{compiled})) {
            my $tmp_name = $special->{compiled};
            debug("Retrieving compiled file $tmp_name for $name\n");
            copy($tmp_name, $name);
        } else {
            my $tmp_name = mktemp("/tmp/${name}_XXXXXX");
            debug("Compiling $name, saving it to $tmp_name\n");
            setup_env($name, $special);
            my $data = run_special($name, '.compile', $special->{compile}, $special, 0);
            if ($data->{failure}) {
                $data->{compilation_error} = 1;
                return $data;
            } else {
                warning("Compilation output : $data->{output}\n") if length($data->{output});
                warning("Compilation error : $data->{error}\n") if length($data->{error});
                copy($name, $tmp_name);
                $special->{compiled} = $tmp_name;
            }
        }
        if (exists($special->{content})) {
            remove("$special->{filename}") unless $special->{keep};
        }
    }
    # Adds limits if any
    if (exists($special->{output_limit}) || exists($special->{error_limit}) || exists($special->{timeout})) {
        $has_limits = 1;
    }
    my $result;
    if (exists($special->{execute})) {
        if ($special->{execute}) {
            setup_env($name, $special);
            $result = run_special($test_name, '.execute', $special->{execute}, $test, $has_limits);
        }
    } else {
        $result = run_command($test_name, "./$name", $test, $has_limits);
    }
    if (exists($special->{content}) && !exists($special->{compile})) {
        remove("$special->{filename}") unless $special->{keep};
    }
    return $result;
}

# Runs a command line for a given test and clean up the outputs if required. Returns a hash that holds
# the content of standard output and error as well as exit code
sub clean_outputs($$) {
    my $data = shift;
    my $test = shift;
    my $level = $test->{cleanup_level};

    $level = 2 unless defined($level);
    foreach my $part ('output', 'error') {
        if ($data->{$part} && ($data->{$part} =~ m/Grade\s*:=>>/i)) {
            $data->{$part} = "Pathetic attempt to set the grade detected\n".
                "Please stop trying to hack the system";
        }
        $data->{$part} = cleanup($data->{$part}, $level);
        $test->{$part} = cleanup($test->{$part}, $level);
    }
}

# Removes extraneous separators for a given string
sub cleanup($$) {
    my $data = shift;
    my $level = shift;
    if (defined($data)) {
        if ($level > 0) {
            # Removes blank lines at the beginning and the end, as well as trailing blanks
            $data =~ s/^\s+\n//;
            $data =~ s/\n\s*$//;
            $data =~ s/\s+$//gm;
            # Removes leading blanks, replaces tabs with spaces and sequences of spaces with a single space
            if ($level > 1) {
                $data =~ s/^\s+//gm;
                $data =~ s/\t/ /gm;
                $data =~ s/ +/ /gm;
                # Join lines with spaces
                if ($level > 2) {
                    $data =~ s/\n/ /g;
                }
            }
        }
    }
    return $data;
}

sub ensure_write($$) {
    my $name = shift;
    my $data = shift;
    if (defined($data)) {
        write_file($name, $data) || error("Cannot write $name");
    }
}

sub add_values($$) {
    my $first = shift;
    my $second = shift;

    foreach my $key (keys(%$second)) {
        $first->{$key} = $second->{$key};
    }
}

sub preformat($) {
    my $line = shift;
    $line =~ s/^/>/gm;
    $line =~ s/[^\p{XPosixPrint}\p{XPosixSpace}]/./g;
    return $line;
}

sub format_error($) {
    my $data = shift;
    my $result = "";
    $result .= "code $data->{code}" if $data->{code};
    if ($data->{signal}) {
        $result .= " and " if $result;
        $result .= "signal $data->{signal}";
    }
    return $result;
}

# runs all tests for a given testcase, returns the score (percentage)
sub perform_tests($$) {
    my $program = shift;
    my $case = shift;
    my $tests = $case->{tests};
    my $score = 0;
    my $total = 0;

    foreach my $test_name (sort(keys(%$tests))) {
        my $break_loop = 0;
        my $test = {};
        add_values($test, $case);
        add_values($test, $tests->{$test_name});

        my $weight = get_value($test, 'weight', 1);
        $total += $weight;
        my $indication = $test_name;
        $indication .= " (weight ".$weight.")" if exists($test->{weight});
        my $result = undef;
        my $details = "Test $test_name:\n";
        # runs setup, if any
        if (exists($test->{setup})) {
            my $setup = run_special($test_name, '.setup', $test->{setup}, $test, 0);
            foreach my $part ('output', 'error') {
                warning("Setup $part :\n$setup->{$part}") if length($setup->{$part});
            }
            dispatch_error("*** POSSIBLE ERROR : Setup exited with ".format_error($setup), $setup, 1)
                if $setup->{failure};
        }

        # runs program
        my $data = run_special($test_name, $program, $test, $test, 1);

        if ($data->{compilation_error}) {
            $details .= "Compilation errors, compiler output :\n".preformat($data->{output}.$data->{error});
            $result = 0;
            $break_loop = 1;
        } else {
            $case->{compiled} = $test->{compiled} if exists($test->{compiled});
            clean_outputs($data, $test);
            $details .= "With the input:\n".preformat($test->{input})."\n" if length($test->{input});
            $details .= "With the arguments: ['".join("','", @{$test->{args}})."']\n" if $test->{args};

            # runs a provided solution for the testcase
            # this will populate test->{output,error,code} for the following test
            if (exists($test->{solution})) {
                my $answer = run_special($test_name, '.solution', $test->{solution}, $test, 0);
                if ($answer->{compilation_error}) {
                    dispatch_error("Cannot compile provided solution", $answer, 0);
                } else {
                    clean_outputs($answer, $test);
                    my $match;
                    if (exists($test->{match})) {
                        $match = $test->{match};
                    } else {
                        $match = [ 'output' ];
                    }
                    foreach my $part ('output', 'error', 'code', 'signal') {
                        if (scalar(grep(/$part/, @$match))) {
                            $test->{$part} = $answer->{$part};
                        } else {
                            if (($part eq 'code') || ($part eq 'signal')) {
                                debug("Solution $part :\n$answer->{$part}\n") if $answer->{$part};
                            } else {
                                debug("Solution $part :\n$answer->{$part}\n") if length($answer->{$part});
                            }
                        }
                    }
                }
            }

            # runs a validator
            if (exists($test->{validator})) {
                ensure_write(".program_name.txt", $program);
                ensure_write(".arguments.txt", join("\n", @{$test->{args}})) if exists($test->{args});
                foreach my $part ('output', 'error', 'code') {
                    ensure_write(".$part.txt", $data->{$part});
                    ensure_write(".expected_$part.txt", $test->{$part});
                }
                my $valid = run_special($test_name, '.validator', $test->{validator}, $test, 0);
                $result = ($valid->{failure} == 0);
                foreach my $part ('output', 'error') {
                    $details .= "Validator $part is:\n".preformat($valid->{$part})."\n" if length($valid->{$part});
                }
                if (!$result) {
                    $details .= "The validator did not validate the test (".format_error($valid).")\n";
                }
                debug "Done with validator subtest with result $result\n";
                remove(".program_name.txt", ".arguments.txt");
                foreach my $part ('output', 'error', 'code') {
                    remove(".$part.txt", ".expected_$part.txt");
                }
            }

            # compare produced output to expected one for each category
            foreach my $part ('output', 'error', 'code', 'signal') {
                my $description = {
                    output => "produced on standard output",
                    error => "produced on standard error",
                    code => "returned the code",
                    signal => "received the signal" };
                if (defined($test->{$part}) || ($data->{$part})) {
                    $details .= "Your program $description->{$part}:\n".preformat($data->{$part});
                    if (defined($test->{$part})) {
                        if ($data->{${part}.'_bounded'}) {
                            $details .=
                                "\nYour program standard $part has reached the upper limit (failure)\n";
                            $result = 0;
                        } else {
                            my $local_result = ($data->{$part} eq $test->{$part});
                            if (!$local_result) {
                                $details .= "\nBut the expectation was:\n".preformat($test->{$part})."\n";
                            } else {
                                $details .= " (as expected)\n";
                            }
                            $result = 1 unless defined($result);
                            $result &&= $local_result;
                            debug "Done with $part subtest with result $local_result\n";
                        }
                    } else {
                        $details .= "\n";
                    }
                    if (!$result && ($part eq 'signal')) {
                        if ($data->{signal} == 9) {
                            $details .= "Looks like a timeout, did you write an infinite loop ?\n";
                        }
                        elsif ($data->{signal} == 13) {
                            $details .= "Looks like your program produces to much output on stdout/stderr.".
                                " This might be caused by an infinite loop.\n";
                        }
                    }
                }
            }
            if (!defined($result)) {
                error("No method defined to check $test_name result !");
                $result = 0;
            }
        }

        # collect result
        if ($result) {
            debug($details);
            comment("* Test $indication PASSED !");
            $score += $weight;
        } else {
            unless ($test->{show}) {
                colored_output('YELLOW', $details);
                $details = "(hidden) ";
            }
            comment("* ".$details."Test $indication FAILED !");
        }
        last if $break_loop;
    }
    return $score / $total;
}

sub evaluate_file($) {
    my $file = shift;
    if (-f $file) {
        my $description = read_file($file) || error("Cannot read $file");
        remove("$file") || error("Unlink of $file resulted in $!");
        my $result = eval $description || error("$file: $@");
        return $result;
    } else {
        return undef;
    }
}


# *** BEGINING OF THE EVALUATION ***
foreach my $arg (@ARGV) {
    $mode{lc($arg)} = 1;
}
hidden("*** Running in local mode ***\n") if $mode{local};
hidden("*** Running in debug mode ***\n") if $mode{debug};
debug("VPL_SUBFILES : $ENV{VPL_SUBFILES}\n");

# read local setup and testcases to run
evaluate_file("vpl_evaluate_setup.pl");
my $test_cases = evaluate_file("vpl_evaluate.cases");

debug(Dumper($test_cases));

my $score = 0;
my $total = 0;

# runs testcases
foreach my $name (sort(keys(%$test_cases))) {
    # The rest is only valid if the element is a test case and not a setting
    if ((ref($test_cases->{$name}) ne 'HASH') || !exists($test_cases->{$name}->{tests})) {
        next;
    }
    my $case = {};
    add_values($case, $test_cases);
    add_values($case, $test_cases->{$name});
    my $program;
    my @files;
    my $weight = get_value($case, 'case_weight', 1);

    my $title = "Test case $name";
    $title .= " (weight ".$weight.")" if exists($case->{case_weight});
    title($title);

    # Tests
    my $test_score = perform_tests($name, $case);
    comment("=> Score for $name : ".($test_score * 100)."/ 100");
    $score += $test_score * $weight;
    $total += $weight;
}
my $min = $ENV{VPL_GRADEMIN};
my $max = $ENV{VPL_GRADEMAX};
if ($total == 0) {
    error("No test defined !");
}
grade($score * $max / $total + $min);
