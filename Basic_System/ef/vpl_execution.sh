#!/bin/bash

if [ -f vpl_common_script.sh ]; then
    . vpl_common_script.sh
fi

# Files to keep until the very last moment
PRECIOUS_FILES="vpl_custom_setup.sh vpl_custom_run.sh"
# VPL_PRECIOUS_FILES can be set in vpl_common_script.sh
# if some files need to be kept until vpl_custom_setup.sh

error_message() {
    message=$1
    text=$2
    if [ "$MODE" != evaluate ]
    then
        echo -e "\033[31m*** $message ***\033[0m $text"
    else
        echo "<|--"
        echo "-$message"
        echo "$text"
        echo "--|>"
    fi
    exit 1
}

internal_error() {
    error_message "Internal Error" "$1"
}

error() {
    error_message "Error" "$1"
}

remove_any() {
    if [ -f $1 ]
    then
        rm $1
    fi
}

search() {
    needle=$1
    shift
    case " $* " in
    *\ $needle\ *) return 0;;
    esac
    return 1
}

# Default settings
if [ -z "$1" ]
then
    internal_error "mode is missing for vpl_execution.sh"
fi
MODE=$1
shift

# Locales are a source of many bad behavior (glob in bash, sort order, ...)
export LC_ALL=C.UTF-8
# But changing locale has an unexpected impact,
# for instance vim doesn't work on the server with C as LC_ALL, here's the fix
echo 'set encoding=utf-8' >>.vimrc
# Note that globasciiranges will be enbaled by default in bash 5.0, this solves half of the issue

# Can cause unwanted extra ouputs if set
unset CDPATH

# If mode is not evaluate, removes any file which is not to generated or
# explicitely precious. We execute this before the custom setup so that
# any file created by the custom setup will not be removed
if [ "$MODE" != evaluate ]
then
    for file in *
    do
        if ! search $file $VPL_SUBFILES $PRECIOUS_FILES $VPL_PRECIOUS_FILES
        then
            rm -fr $file
        fi
    done
    # adjust the terminal size to the actual window
    resize >/dev/null
fi

# Runs custom setup (common to all modes)
if [ -f vpl_custom_setup.sh ]
then
    source ./vpl_custom_setup.sh
    echo "Completed custom setup"
fi

# Runs custom run (to compile things or decide on the executable)
if [ -f vpl_custom_run.sh ]
then
    source ./vpl_custom_run.sh
    echo "Completed custom run"
fi

# If mode is not evaluate
if [ "$MODE" != evaluate ]
then
    # Removes precious files
    for file in $PRECIOUS_FILES
    do
        if [ -f $file ]
        then
            rm $file
        fi
    done
    # Runs appropriately depending on the mode and some variables
    case $MODE in
        run)
            if [ -n "$RUN" ]
            then
                eval exec "$RUN" $ARGUMENTS
            else
                exec bash
            fi
            ;;
        debug)
            if [ -n "$DEBUG" ]
            then
                eval exec "$DEBUG" $ARGUMENTS
            else
                echo "set -x" >.bashrc
                exec bash
            fi
            ;;
        *)
            internal_error  "Invalid mode"
            ;;
    esac
else
    if [ ! -f vpl_evaluate.pl ]
    then
        internal_error "vpl_evaluate.pl missing"
    fi
    chmod +x vpl_evaluate.pl || internal_error "can't make vpl_evaluate.pl executable"
    exec ./vpl_evaluate.pl $* $EVALUATE_MODE || internal_error "can't exec vpl_evaluate.pl"
fi
