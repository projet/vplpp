#!/bin/bash
C_FILES=0
export CC=${CC-'gcc'}
export CPPFLAGS=${CPPFLAGS-'-Wall -Werror'}
export CFLAGS=${CFLAGS-'-g'}
export LDFLAGS=${LDFLAGS-''}
export WEAKEN_SYMBOLS

compile() {
    for file in $*
    do
        base=$(basename "$file" .c)
        if [ "$base" != "$file" -a ! -f "$base.o" ]
        then
            echo "Compiling $file..."
            echo $CC $CPPFLAGS $CFLAGS -c "$file"
            $CC $CPPFLAGS $CFLAGS -c "$file" || return 1
            echo " done."
            C_FILES=1
        fi
    done
}

if [ "$MODE" != evaluate ]
then
    # Try to compile existing files
    if [ -f Makefile ]
    then
        FILES_LIST=*
        make
        objects=$(ls *.o 2>/dev/null)
        if [ -n "$objects" ]
        then
            rm $objects
        fi
        for file in *
        do
            if ! search $file $FILES_LIST
            then
                GENERATED_FILES="$GENERATED_FILES $file"
            fi
        done
    else
        compile $VPL_SUBFILES || error "Compilation error"
#        compile *.c >/dev/null 2>&1 || internal_error "Compilation of a hidden file failed"
        if [ $C_FILES -gt 0 ]
        then
            $CC $LDFLAGS *.o -o main_program $LDLIBS || error "Linking into main_program failed"
            GENERATED_FILES=main_program
            RUN=${RUN-'./main_program'}
            DEBUG=${DEBUG-'gdb --args main_program'}
        fi
    fi
fi
