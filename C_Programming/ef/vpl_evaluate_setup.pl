my $compilation_code = q{
search() {
    needle=$1
    shift
    for hay in $*
    do
        if [ "$needle" = "$hay" ]
        then
            return 0
        fi
    done
    return 1
}

end() {
    if [ $(expr "$-" : '.*x') -eq 0 -a -n "$REMOVE$GENERATED_FILES" ]
    then
        rm -r $REMOVE $GENERATED_FILES
    fi
    exit $1
}

exit_code=0
if [ -z "$FILES" ]
then
    FILES=*.c
fi
for file in $FILES
do
    base=$(basename $file .c)
    $CC $CPPFLAGS $CFLAGS -c $file || end $?
    GENERATED_FILES="$GENERATED_FILES $base.o"
    if [ -n "$WEAKEN_SYMBOLS" ] && search $file $VPL_SUBFILES
    then
        weaken_list=""
        for symbol in $WEAKEN_SYMBOLS
        do
            weaken_list="$weaken_list -W $symbol"
        done
        objcopy $weaken_list $base.o
    fi
done
$CC $LDFLAGS $GENERATED_FILES $LDLIBS -o $NAME || end $?
end 0
};

sub C_compilation(@) {
    return "#!/bin/bash\n".join('\n',@_)."\n".$compilation_code;
}

sub C_function_call($) {
    my $name = shift;
    return "#!/bin/bash\nfunction=$name\n".q{
dir=$(mktemp -d /tmp/programXXXXXX)
FILES="$FILES $dir/main.c"
REMOVE="$dir"
cat >$dir/main.c <<END
void $function();

int main() {
    $function();
    return 0;
}
END
}.$compilation_code;
}
