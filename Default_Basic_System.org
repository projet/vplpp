#+OPTIONS: num:nil
#+OPTIONS: toc:nil
#+OPTIONS: html-postamble:nil
#+OPTIONS: html-preamble:nil
#+PROPERTY: header-args :eval never :mkdirp yes :exports none
# Doesn't work with include (can't override default)
#+LANGUAGE: fr
#+EXPORT_EXCLUDE_TAGS: noexport

Fichier servant uniquement à mettre à jour l'activité par défaut associée à cette activité.

#+BEGIN_SRC txt :tangle Default_Basic_System/vpl_id.txt
23947
#+END_SRC

#+BEGIN_SRC shell :eval yes
  #!/bin/bash
  make Basic_System/vpl_id.txt
  rm -r Default_Basic_System
  mkdir -p Default_Basic_System
  cp -r Basic_System/* Default_Basic_System
  rm Default_Basic_System/push
#+END_SRC
