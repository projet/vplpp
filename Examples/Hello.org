* Réglages :noexport:

#+OPTIONS: num:nil
#+OPTIONS: toc:nil
#+OPTIONS: html-postamble:nil
#+OPTIONS: html-preamble:nil
#+LANGUAGE: fr
#+EXPORT_EXCLUDE_TAGS: noexport
#+PROPERTY: header-args :eval never :mkdirp yes :exports none

#+BEGIN_SRC txt :tangle Hello/vpl_id.txt
26126
#+END_SRC

#+BEGIN_SRC perl :tangle Hello/ef/vpl_evaluate.cases
  # On initialise une table de hachage vide
  my $cases = {};
#+END_SRC

* Hello world
Écrire un programme qui affiche ='Hello world !'=.

#+BEGIN_SRC shell :tangle Hello/cf/hello_world.sh
  echo "Hello world !"
#+END_SRC

#+BEGIN_SRC perl :tangle Hello/ef/vpl_evaluate.cases :output append
  # Ajout du premier cas de test
  $cases->{"hello_world.sh"} = {
      tests => {
          test_1 => { show => 1, output => "Hello world !" }
      }
  };
#+END_SRC

* Salut monde
Écrire un programme qui affiche ='Salut monde !'=.

#+NAME: solution_salut
#+BEGIN_SRC shell :tangle Hello/cf/salut_monde.sh
  echo "Salut monde !"
#+END_SRC

#+BEGIN_SRC perl :tangle Hello/ef/vpl_evaluate.cases :output append :noweb yes
  # l'opérateur q{} marcherait aussi, mais le 'here document' avec quotes est la
  # forme d'inclusion la moins sensible à la présence de caractères d'échappement
  my $solution = <<'END';
<<solution_salut>>
END

  # Ajout du second cas de test
  $cases->{"salut_monde.sh"} = {
      solution => $solution,
      tests => {
          test_1 => {}
      }
  };
#+END_SRC

* Epilogue :noexport:

#+BEGIN_SRC perl :tangle Hello/ef/vpl_evaluate.cases :output append
  # suppression d'un ou plusieurs cas de test, utile lors du developpement de l'activité
  # delete($cases->{"hello_world.sh"});
  # delete($cases->{"salut_monde.sh"});
  $cases;
#+END_SRC
