DOCS_ONLY=README.org Methodology.org Command_Line.org Local_Execution.org
ACTIVITIES=$(patsubst %.org, %, $(filter-out $(DOCS_ONLY), $(wildcard *.org)))
ALL_VPL_IDS=$(addsuffix /vpl_id.txt, $(ACTIVITIES))
VPL_IDS=$(filter-out Default_%, $(ALL_VPL_IDS))
DEFAULT_VPL_IDS=$(filter Default_%, $(ALL_VPL_IDS))
PUSHES=$(addsuffix /push, $(ACTIVITIES))
DEBUGS=$(addsuffix /debug, $(ACTIVITIES))

EMACS=emacs --batch --eval "(progn \
	(setq enable-local-eval t) \
	(setq enable-local-variables t) \
	(require 'org) \
	(setq org-export-babel-evaluate t) \
	(setq org-confirm-babel-evaluate nil) \
	(setq org-latex-listings 'listings) \
    (org-babel-do-load-languages 'org-babel-load-languages '(\
        (shell . t)\
    )) \
	(setq org-src-preserve-indentation t)\
)"

.PHONY: all debug default files pushes $(DEBUGS)

all: files

.SECONDEXPANSION:
%/vpl_id.txt: %.org $$(wildcard %/*/*)
	$(EMACS) $< --batch --funcall org-babel-execute-buffer
	$(EMACS) $< --batch --funcall org-babel-tangle

$(DEFAULT_VPL_IDS): Default_%/vpl_id.txt: %/vpl_id.txt

.SECONDEXPANSION:
%/push: %/vpl_id.txt $$(wildcard %/*/*)
	cd $* && ../local.sh push
	touch $@

.SECONDEXPANSION:
$(DEBUGS): %/debug: $$(wildcard %/*/*)
	echo $^

debug:
	@echo "ACTIVITIES: $(ACTIVITIES)"
	@echo "VPL_IDS: $(VPL_IDS)"
	@echo "DEFAULT_VPL_IDS: $(DEFAULT_VPL_IDS)"

files: $(VPL_IDS)

push: $(PUSHES)

default: $(DEFAULT_VPL_IDS)
