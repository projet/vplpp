#!/usr/bin/env bash
VPLPP=$(realpath $(dirname $0))
VPLADMIN=$VPLPP/vpladmin

usage () {
    cat <<EOF
Usage:
$0 evaluate|run|debug [debug]
$0 push
EOF
    exit 1
}

if [ -z "$1" ]
then
    usage
else
    MODE=$1
    shift
fi

if [ $MODE != push ]
then
    if [ -d local_run ]
    then
        rm -fr local_run
    fi
    export VPL_SUBFILES=$(find cf rf hf -maxdepth 1 -printf "%P\n" 2>/dev/null | sort | uniq)
    export VPL_GRADEMIN=${VPL_GRADEMIN-0.0}
    export VPL_GRADEMAX=${VPL_GRADEMAX-100.0}
    local_files=$(echo rf/* cf/* hf/* ef/*)
    mkdir local_run
    for file in *.sh *.pl $local_files
    do
        if [ -f "$file" ]
        then
            cp -v $file local_run
        fi
    done
    cd local_run

    chmod +x ./vpl_execution.sh
    exec ./vpl_execution.sh $MODE local $*
else
    set -e
    ID=$(cat vpl_id.txt)
    dest=pushed_files
    mkdir -p $dest
    for dir in ef cf rf
    do
        if [ -d $dir ]
        then
            cp -r $dir $dest
        else
            mkdir -p $dest/$dir
        fi
    done
    for file in $(ls cf 2>/dev/null)
    do
        touch $dest/rf/$file
    done
    for file in $(ls hf 2>/dev/null)
    do
        cp hf/$file $dest/cf
    done
	if [ -f intro.html ]
	then
		$VPLADMIN --id $ID set_setting intro:intro.html
	fi
	cd $dest
	typeset -A category
	category[ef]=execution_files
	category[cf]=corrected_files
	category[rf]=required_files
	for dir in ef cf rf
	do
		files=$(ls $dir 2>/dev/null)
		if [ -n "$files" ]
		then
			cd $dir
			$VPLADMIN --id $ID save_${category[$dir]} $files
			cd ..
		fi
	done
	cd ..
    rm -r $dest
fi
