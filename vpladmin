#!/usr/bin/perl
use strict;
use LWP::UserAgent;
use File::Slurp;
use Getopt::Long qw(:config bundling);

my $url = 'https://moodle.caseine.org';
my $path = 'webservice/rest/server.php';
my $debug=0;
my $json=0;
my $vplid=$ENV{'VPLADMIN_ID'};
my $token=$ENV{'VPLADMIN_TOKEN'};
$url=$ENV{'VPLADMIN_SERVER'} if exists($ENV{'VPLADMIN_SERVER'});
$path=$ENV{'VPLADMIN_PATH'} if exists($ENV{'VPLADMIN_PATH'});

sub help();
sub failure($);

sub debug($) {
    print shift if ($debug);
}

sub save($@) {
    my $content = shift;
    my $num=0;
    failure "Missing files" unless scalar(@_);
    foreach my $file (@_) {
	debug("Adding $file...\n");
	my $data = read_file($file);
	push @{$content}, "files[$num][name]", $file, "files[$num][data]", $data;
	$num++;
    }
}

sub get_setting($@) {
    my $content = shift;
    my $setting = shift;

    failure "Extraneous arguments" unless not scalar(@_);
    push @{$content}, "settingname", $setting;
}

sub set_setting($@) {
    my $content = shift;
    my $setting = shift;

    failure "Extraneous arguments" unless not scalar(@_);
    my ($name, $value);
    if ($setting =~ m/^\w+=.*$/) {
	($name, $value) = split /=/, $setting, 2;
    }
    if ($setting =~ m/^\w+:.*$/) {
	($name, $value) = split /:/, $setting, 2;
	($value = read_file($value)) || failure "Cannot read file $value";
    }
    failure "Invalid setting" unless defined($value);
    push @{$content}, "settingname", $name, "settingvalue", $value;
}

sub void($@) {
}

my %handlers = (
    'save' => \&save,
    'save_required_files' => \&save,
    'save_corrected_files' => \&save,
    'save_execution_files' => \&save,
    'get_setting' => \&get_setting,
    'set_setting' => \&set_setting,
    'open' => \&void,
    'evaluate' => \&void,
    'get_result' => \&void,
    'info' => \&void
    );
my @commands = keys(%handlers);

sub help() {
    print <<"END";
usage :
$0 [ --help ] [ --json ] [ --server url ] [ --path path ] [ --token value ] [ --id value ] command [ arguments ]

Pushes data to caseine, where :
- server is the url of the server hosting the webservice (by default it is 'https://moodle.caseine.org' or VPLADMIN_SERVER if defined)
- path is the path on the server at which one can find the webservice (by default it is 'webservice/rest/server.php' or VPLADMIN_PATH if defined)
- token is the personnal token associated to the user of this script (by default, the value of the VPLADMIN_TOKEN environment variable)
- id is the id number of the involved VPL (by default, the value of the VPLADMIN_ID environment variable)
- the output is in JSON format is the --json option is given
- command is one of : @commands and its arguments are :
  - filenames for save commands
  - a setting name for get_setting
  - an assignment of the form name=value or name:filename for set_setting
  - nothing for the other ones

More informations can be found at : https://moodle.caseine.org/mod/wiki/view.php?pageid=199
END
    exit(0);
}

sub failure($) {
    print "@_\n";
    help;
}

GetOptions('help|h|?' => \&help,
	   'server|s=s' => \$url,
	   'path|p=s' => \$path,
	   'id|i=s' => \$vplid,
	   'token|t=s' => \$token,
	   'debug|d' => \$debug,
	   'json|j' => \$json
    );
#failure "Missing command" if scalar(@ARGV) == 0;

debug "SERVER = $url, PATH = $path, VPLID = $vplid, TOKEN = $token, ARGS = @ARGV\n";
$url .= '/'.$path;

failure "Missing token" unless defined($token);
failure "Missing VPL ID" unless defined($vplid);

my $command = shift @ARGV;
my $content = [
    id         => "$vplid",
    wstoken    => "$token",
    wsfunction => "mod_vpl_$command"
    ];
push @{$content}, "moodlewsrestformat", "json" if $json;
failure "Unknown command $command" unless exists($handlers{$command});
&{$handlers{$command}}($content, @ARGV);

my $ua = LWP::UserAgent->new( agent => 'Secret');
$ua->ssl_opts(verify_hostname => 0);
$ua->ssl_opts(SSL_verify_mode => 0x00);
my $res = $ua->post( $url,
		     Content_Type => 'form-data',
		     Content => $content
    );

if (exists($res->{'_content'})) {
    my $answer = $res->{'_content'};
    print $answer;
} else {
    failure "Invalid answer from server";
}
